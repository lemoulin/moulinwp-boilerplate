module.exports = {
  plugins: [
    require('autoprefixer'),
    require('postcss-clip-path-polyfill')
  ]
}
