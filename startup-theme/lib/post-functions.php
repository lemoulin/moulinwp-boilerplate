<?php

/**
 * Return <img/div> markup using WP's media image object array
 */
function get_responsive_image($image_object = null, $size = 'medium', $type = 'lazy', $pixel = false) {
  global $post;

  // handle image source
  if ( !$image_object && has_post_thumbnail() ) {
    $image = get_the_post_thumbnail_url($post);
  } else if ($image_object && isset($image_object['sizes'])) {
    $image = $image_object['sizes'][$size];
  } else {
    // no object and no thumbnail, returns nothing
    return false;
  }

  // with pixel image
  if ($pixel) {
    $pixel_img = "<img src='{$image_object['sizes']['pixel']}' class='preload-pixel' />";
  } else {
    $pixel_img = null;
  }

  // output markup
  switch ($type) {
    case 'lazy':
      $img =  "<img src='{$pixel}' class='b-lazy' data-src='{$image}' />";
      break;
    case 'bg':
      $img =  "<picture class='b-lazy bg-filled' data-src='{$image}'></picture>";
      break;
    case 'inline':
      $img =  "<img src='{$image}' />";
  }

  return $img;
}


/**
 * Returns row odd/even fraction
 */

function row_fraction($index) {
  return ($index % 2) == 1 ? 'odd' : 'even';
}


/**
 * Returns object transition speed
 */
function object_transition_stagger_speed($index = 0, $coeficient = 4, $base = 0,  $every = 3, $timing = 's') {
  // recalculate index when reached X loop iteration
  $i = $index % $every;
  $speed = $base + ($i / $coeficient);
  return sprintf("%s%s", $speed, $timing);
}

/**
 * Check if on last page of a wp_query object
 */

function is_last_page() {
  global $wp_query;

  $page        = get_query_var('paged');
  $total_page  = $wp_query->max_num_pages;

  // return bool
  return ($page < $total_page) ? false : true;
}


/**
 * Change read more link
 */

function excerpt_more_custom() {
  global $post;
  return '<nav class="read-more"><a class="link--learn-more" href="' . get_permalink($post->ID) . '">' . __("Read more", "moulinwp") . '<i class="ion-ios-arrow-down"></i></a></nav>';
}

add_filter( 'excerpt_more', 'excerpt_more_custom' );

?>
