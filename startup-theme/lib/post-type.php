<?php

namespace MoulinWP\PostType;

/**
 * Creating a function to create our Custom Post Type
 */
class Theme_CustomPostTypes {

  public $theme_domain = 'moulinwp';
  public $menu_position = 3; // starting position in admin menu

  function __construct() {
    $this->projects();
  }

  private function incrementMenuPosition() {
    return $this->menu_position += 1;
  }

  // Create projects post-types
  public function projects() {
    $labels = array(
        'name'                => __("Projects"),
        'singular_name'       => __("Project"),
        'menu_name'           => __( 'Projects', $this->theme_domain ),
        'parent_item_colon'   => __( 'Parent Project', $this->theme_domain ),
        'all_items'           => __( 'All Projects', $this->theme_domain ),
        'view_item'           => __( 'View Project', $this->theme_domain ),
        'add_new_item'        => __( 'Add New Project', $this->theme_domain ),
        'add_new'             => __( 'Add New', $this->theme_domain ),
        'edit_item'           => __( 'Edit Project', $this->theme_domain ),
        'update_item'         => __( 'Update Project', $this->theme_domain ),
        'search_items'        => __( 'Search Project', $this->theme_domain ),
        'not_found'           => __( 'Not Found', $this->theme_domain ),
        'not_found_in_trash'  => __( 'Not found in Trash', $this->theme_domain ),
    );

    // Set other options for Custom Post Type
    $args = array(
        'label'               => __( 'projects', $this->theme_domain ),
        'description'         => __( 'Project details', $this->theme_domain ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'editor', 'revisions' ),
        'hierarchical'        => true,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => $this->incrementMenuPosition(),
        'can_export'          => true,
        'has_archive'         => false,
        'exclude_from_search' => true,
        'publicly_queryable'  => true,
        'capability_type'     => 'post',
        // 'yarpp_support'       => true,
        'menu_icon'           => 'dashicons-portfolio',
        'rewrite'             => array('slug' => 'projects')
    );

    // Register
    register_post_type( 'projects', $args );
  }

}

new Theme_CustomPostTypes();


?>
