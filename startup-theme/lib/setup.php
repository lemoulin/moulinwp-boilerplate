<?php

namespace MoulinWP\Setup;

use MoulinWP\Assets;

/**
 * Theme setup
 */
function setup() {
  // Enable features from Soil when plugin is activated
  // https://roots.io/plugins/soil/
  add_theme_support('soil-clean-up');
  add_theme_support('soil-nav-walker');
  add_theme_support('soil-nice-search');
  add_theme_support('soil-jquery-cdn');
  add_theme_support('soil-relative-urls');

  // Make theme available for translation
  // Community translations can be found at https://github.com/roots/sage-translations
  load_theme_textdomain('sage', get_template_directory() . '/lang');

  // Enable plugins to manage the document title
  // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
  add_theme_support('title-tag');

  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus
  register_nav_menus([
    'primary_navigation' => __('Primary Navigation', 'franksullivan'),
    'secondary_navigation' => __('Secondary Navigation', 'franksullivan'),
    'social_links' => __('Social links', 'franksullivan')
  ]);

  // Enable post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
  add_theme_support('post-thumbnails');
  add_image_size( 'pixel', 100 );
  add_image_size( 'large', 1024 );
  add_image_size( 'largest', 1800 );
  add_image_size( 'largest-retina', 2800 );

  // Enable post formats
  // http://codex.wordpress.org/Post_Formats
  // add_theme_support('post-formats', ['aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio']);

  // Enable HTML5 markup support
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
  add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

  // Use main stylesheet for visual editor
  // To add custom styles edit /assets/styles/layouts/_tinymce.scss
  // add_editor_style(Assets\asset_path('styles/main.css'));

  // options page for ACF
  if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
      'page_title' 	=> 'Options du thème',
      'menu_title'	=> 'Options thème',
      'menu_slug' 	=> 'theme-general-settings'
    ));
  }

}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');


/**
 * Admin scripts
 */
function franksullivan_admin_load_scripts() {
    // add Google Maps API key to ACF
    add_filter('acf/settings/google_api_key', function () {
        return 'AIzaSyA8t5YdT6OFDyT0Nz7SCrjfVMZh59ot8dc';
    });
}

add_action( 'admin_enqueue_scripts', __NAMESPACE__ . '\\franksullivan_admin_load_scripts', 100 );

/**
 * Register sidebars
 */
function widgets_init() {
  register_sidebar([
    'name'          => __('Newsletter', 'franksullivan'),
    'id'            => 'sidebar-newsletter',
    'before_widget' => '<section class="widget %1$s %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>'
  ]);
}
add_action('widgets_init', __NAMESPACE__ . '\\widgets_init');

/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar() {
  static $display;

  isset($display) || $display = !in_array(true, [
    // The sidebar will NOT be displayed if ANY of the following return true.
    // @link https://codex.wordpress.org/Conditional_Tags
    is_404(),
    is_front_page(),
    is_page_template('template-custom.php'),
  ]);

  return apply_filters('sage/display_sidebar', $display);
}

/**
 * Theme assets
 */
 /**
  * Theme assets
  */
 function assets() {
   if ( function_exists('pll_current_language')) {
     $language = pll_current_language('slug');
   } else {
     $language = 'en';
   }

   wp_enqueue_script('franksullivan/gmaps', "https://maps.googleapis.com/maps/api/js?key=AIzaSyA8t5YdT6OFDyT0Nz7SCrjfVMZh59ot8dc&libraries=places&language={$language}", null, true, true);

  // webpack dev
  if (THEME_DEV === true) {
    wp_enqueue_script('franksullivan/webpack', 'http://localhost:8080/js/bundle.js', null, null, true);
  } else {
    wp_enqueue_style('franksullivan/css', Assets\asset_path('css'), false, null);
    wp_enqueue_script('franksullivan/js', Assets\asset_path('js'), [], null, true);
  }

  // remove core scripts and freaking emoji
  remove_action('wp_head', 'print_emoji_detection_script', 7);
  remove_action('wp_print_styles', 'print_emoji_styles');
  wp_deregister_script('wp-embed');
  wp_deregister_script( 'jquery' );
 }

 add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\assets', 100);
