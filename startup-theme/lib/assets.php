<?php

namespace MoulinWP\Assets;

/**
 * Get paths for assets
 */
class JsonManifest {
  private $manifest;

  public function __construct($manifest_path) {
    $this->manifest = [];

    // echo $manifest_path;

    if (file_exists($manifest_path)) {
      $this->manifest = json_decode(file_get_contents($manifest_path), true);
    } else {
      $this->manifest = null;
    }
  }

  public function get() {
    return $this->manifest;
  }

}

function get_file_path($filename) {
  $dir = get_template_directory_uri();
  $src_dir = "/src/";
  return $dir . $src_dir . $filename;
}

function asset_path($filename) {
  $dist_path = get_template_directory_uri();
  $directory = dirname($filename) . '/';
  $file = basename($filename);
  static $manifest;

  if (empty($manifest)) {
    $manifest_path = get_template_directory() . '/dist/' . 'assets.json';
    $manifest = new JsonManifest($manifest_path);
  }

  // echo is_null($manifest);

  if ($manifest->get() && array_key_exists($filename, $manifest->get()['main'])) {
    return $manifest->get()['main'][$filename];
  } else {
    return $dist_path . $directory . $file;
  }
}
