import domready from 'domready';
import Blazy from 'blazy/blazy.js';
import WOW from 'wow.js/dist/wow.min';
import { forEach } from 'lodash';

export const bLazyInstance = new Blazy({
  offset: 1500,
  loadInvisible: true
});

class Figures {

  constructor() {
    this.init();
  }

  init() {
    domready( ()=> {
      this.lazyload();
      this.reveals();
    });
  }

  lazyload() {
    bLazyInstance.revalidate();
  }

  reveals() {
    new WOW().init();
  }

}


export default Figures;
