import Nav from './Nav';
import Header from './Header';
import Figures from './Figures';

const components = {
  'Nav': new Nav(),
  'Header': new Header(),
  'Figures': new Figures()
};

// method for a global reinit
components.init = () => {
  components.Nav.init();
  components.Header.init();
  components.Figures.init();
}


export default components;
