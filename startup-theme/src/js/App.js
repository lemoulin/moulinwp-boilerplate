import 'babel-polyfill';
import 'jquery/dist/jquery.slim';
import domready from 'domready';
import Barba from 'barba.js';
import Rellax from "rellax";

import components from 'components';
import modules from 'modules';
import controllers from 'controllers';
// import { setMobileClasses } from 'core/Mobile';
import DOMRouter from 'core/DOMRouter';

// Defines the router
let router = new DOMRouter(controllers);

/*
 * Main app
 */
class App {

  constructor() {
    this.init();
  }

  init() {
    router.init();
    this.ready();
    this.transport();
  }

  ready() {
    // do something on initial load
  }

  transport() {
    Barba.Pjax.cacheEnabled = false;
    Barba.Pjax.start();

    // will store new page class-names
    let bodyClasses = null;

    Barba.Dispatcher.on('newPageReady', (currentStatus, prevStatus, HTMLElementContainer, newPageRawHTML) => {
      // parse source as an html document
      var parser = new DOMParser();
      let source = parser.parseFromString(newPageRawHTML, "text/html");

      // grab new body classes and store them
      bodyClasses = source.body.classList.value;
    });

    // set reference to DOM Init call
    let DOMInit = () => {
      this.DOMInit();
    };

    let PageTransition = Barba.BaseTransition.extend({

      initialClassName  : 'site--loading',
      loadingClassName  : 'page--loading',
      leavingClassName  : 'page--leaving',
      betweenClassName  : 'page--between',
      enteringClassName : 'page--entering',

      start: function() {
        Promise
          .all([this.newContainerLoading, this.leaving()])
          .then(this.between.bind(this))
          .then(this.entering.bind(this))
          .then(this.completed.bind(this));
      },

      leaving: function() {
        // deregister window event
        $(window).off('scroll resize');

        // remove old entering class
        $('html').removeClass(this.initialClassName);
        $('html').removeClass(this.enteringClassName);
        $('html').removeClass(this.betweenClassName);
        $('html').removeClass(this.leavingClassName);

        // set transition state class
        $('html').addClass(this.loadingClassName);
        $('html').addClass(this.leavingClassName);

        // set old container in loading state
        $(this.oldContainer).css({
          zIndex: 1,
          position: 'absolute',
          top: 0 + parseInt($("body").css('paddingTop')),
          left: 0,
          width: '100%'
        });

        return $(this.oldContainer).animate({'visibility': 'visible'}, 250).promise();
      },

      between : function() {
        $(this.newContainer).css({
          visibility : 'visible',
          zIndex: 2
        });

        // init new page
        DOMInit();

        // set transition state class
        $('html').addClass(this.betweenClassName);

        // set new classes on body
        document.body.classList = bodyClasses;

        // // scroll page back to top
        window.scrollTo(0, 0);

        // delay the Promise with an dummy animation
        return $(this.oldContainer).animate({'visibility': 'visible'}, 500).promise();
      },

      entering: function() {
        // set transition state class
        $('html').removeClass(this.betweenClassName);
        $('html').addClass(this.enteringClassName);

        // just return promise
        return $(this.newContainer).promise();
      },

      completed: function() {
        // remove body class
        $('html').removeClass(this.loadingClassName);
        $('html').removeClass(this.leavingClassName);

        // set Promise as done
        return this.done();
      }

    });

    Barba.Pjax.getTransition = function() {
      return PageTransition;
    };

  }

  // re init modules and components, GA and all...
	DOMInit() {
    components.init();
    modules.init();
		router.init();

    // send page view to GA
    try {
      ga('send', 'pageview', {
        'page': window.location.pathname
      });
    } catch (e) {
      console.warn(e, 'No Google Analytics Code installed');
    }
	}

}


/*
 * Start app
 */
domready(() => {
  new App();
});
