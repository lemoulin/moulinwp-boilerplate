// lib
import domready from 'domready';

// import modules
import Accordion from "./Accordion";
import Modals from './Modals';
import PageSpy from './PageSpy';

const modules = {
  "Accordion": new Accordion(),
  "Modals": new Modals(),
  "PageSpy": new PageSpy()
};

// method for a global reinit
modules.init = () => {
  modules.Accordion.init();
  modules.Modals.init();
  modules.PageSpy.init();
}


export default modules;
