import domready from 'domready';

class Modals {

  constructor() {
    this.init();
  }

  init() {
    domready( ()=> {
      this.bindEvents();
    });
  }

  bindEvents() {

    // custom events
    let onModalOpen = new Event('onModalOpen');
    let onModalClose = new Event('onModalClose');

    // init all modals
    $('.modal').each((i, elem) => {
      // get type
      let modalType = $(elem).data('modal-type');

      elem.addEventListener('onModalOpen', (event) => { this.open(event, modalType); }, false);
      elem.addEventListener('onModalClose', (event) => { this.close(event, modalType) }, false);

      // close button in modal
      $(elem).find('[data-modal-close]').on('click', (event)=> {
        event.preventDefault();
        elem.dispatchEvent(onModalClose);
      });
    });

    // open events
    $(document).on('click', '[data-modal-open]', (event) => {
      event.preventDefault();

      // get element ID
      let $e = $(event.currentTarget);
      let id = $e.attr('href').replace('#', '');

      // find element and fire event
      document.getElementById(id).dispatchEvent(onModalOpen);
    });
  }

  open(event, modalType) {
    let elem = event.target;
    $(elem).addClass('is-active');

    // init based on modal-type
    switch (modalType) {
      case 'document':
        this.previewDocument(elem)
        break;
      case 'video':
        this.startVideo(elem);
        break;
    }
  }

  close(event, modalType) {
    let elem = event.target;
    $(elem).removeClass('is-active');

    // close event based on modal-type
    switch (modalType) {
      case 'video':
        this.pauseVideo(elem);
        break;
    }
  }

  previewDocument(elem) {
    let documentURL = $(elem).data('document-url');
    let $target     = $(elem).find('.modal-card-body').empty();
    var $iframe = $("<div/>").addClass('google-doc').html('<iframe src="https://docs.google.com/viewer?url='+documentURL+'" width="100%" height="600" style="border: none;"></iframe>');
    $iframe.appendTo( $target );
  }

  startVideo(elem) {
    let video = elem.getElementsByTagName('video')[0];

    // change preload to auto, then start the video
    video.preload = "auto";
    video.play();
    // if (video && video.readyState == 4) {
    //   video.play();
    // }
  }

  pauseVideo(elem) {
    let video = elem.getElementsByTagName('video')[0];
    if (video) {
      video.pause();
    }
  }

}

export default Modals;
