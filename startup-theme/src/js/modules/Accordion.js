const accordions = "[data-accordion]";

class Accordion {

  constructor() {
    this.init();
  }

  init() {
    this.bindEvents();
  }

  bindEvents() {
    $(accordions).each((index, e) => {
      // find elements
      let $toggle = $(e).find('.accordion--item--toggle');
      let $e      = $(e);

      // trigger button
      $toggle.on('click', (event)=> {
        event.preventDefault();
        $toggle.toggleClass('open');
        $e.toggleClass('open');
      });
    });
  }
}

export default Accordion;
