
<section id="home">

  <article <?php post_class(); ?>>
    <h1 class="title is-1"><?php the_title() ?></h1>
    <?php the_content() ?>
  </article>

</section>
