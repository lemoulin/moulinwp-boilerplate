<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <?php wp_head(); ?>

  <?php if (function_exists('pll_current_language')): ?>
  <script type="text/javascript">
  // constants
  var LANGUAGE = "<?= pll_current_language('slug') ?>";
  </script>
  <?php endif; ?>

</head>
