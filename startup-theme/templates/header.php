<header id="site-header" class="banner">

  <div class="container-fluid">
    <div class="level">

      <aside class="level-left">
        <a class="brand" href="<?= esc_url(home_url('/')); ?>"><?php bloginfo('name'); ?></a>
      </aside>

      <nav class="level-right nav-primary">
        <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'is-inlined']);
        endif;
        ?>
      </nav>
    </div>
  </div>
</header>
