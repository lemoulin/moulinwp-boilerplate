import path from 'path';
import env from 'get-env';
import webpack from 'webpack'; // to access built-in plugins
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import AssetsPlugin from 'assets-webpack-plugin';
import CleanWebpackPlugin from 'clean-webpack-plugin';
import DirectoryNamedWebpackPlugin from 'directory-named-webpack-plugin';

// get NODE_ENV shell variable, define if we are in debug mode or production
const isDebug = env() === 'dev' ? true : false;
const isReact = false;

// dev server
let server_port = 8080;

// returns theme folder name
let themeFolderName = () => {
  let parts = __dirname.split('/');
  return parts[parts.length - 1];
};

// source paths
let src_path    = path.join(__dirname, './src');
let sass_path   = path.resolve(src_path, 'sass');
let app_path    = path.resolve(src_path, 'js');

// dist paths
let dist_path   = path.resolve('./dist');
let js_path     = path.resolve(dist_path, 'js');
let css_path    = path.resolve(dist_path, 'css');

// relative theme path
let public_path = `/wp-content/themes/${themeFolderName()}/dist/`;

// CSS extraction
const extractCSS = new ExtractTextPlugin({
    filename: 'css/[name].[hash].css',
    disable: false,
    allChunks: true
});

// webpack config
const config = {
  cache: true,
  context: __dirname,

  resolve : {
    alias: {
      modules: path.resolve(__dirname, 'src/js/modules/'),
      components: path.resolve(__dirname, 'src/js/components/'),
      controllers: path.resolve(__dirname, 'src/js/controllers/'),
      core: path.resolve(__dirname, 'src/js/core/')
    }
  },

  entry: [
    // include hot reloading stuff only in debug mode
    ...(isDebug && !isReact ? [
      `webpack-dev-server/client?http://localhost:${server_port}`,
      'webpack/hot/only-dev-server',
    ] : []),

    // includ react-hot-loader
    ...(isDebug && isReact ? [
      'react-hot-loader/patch',
      `webpack-dev-server/client?http://localhost:${server_port}`,
    ] : []),

    // main assets
    path.join(app_path, 'App.js'),
    path.join(sass_path, 'App.sass')
  ],

  output: {
    path: dist_path,
    filename: isDebug ? 'js/bundle.js' : 'js/bundle.[hash].js',
    publicPath: isDebug ? `http://localhost:${server_port}/` : public_path
  },

  ...(isDebug ? {
    // devtool: 'inline-source-map',
    devtool: '#eval-source-map',
    devServer: {
      hot: true,
      contentBase: dist_path,
      headers: { "Access-Control-Allow-Origin": "*" },
      publicPath: isDebug ? `http://localhost:${server_port}/` : null
    }
  } : {}),

  module: {
    rules: [
      { test: /\.(js|jsx)$/, use: 'babel-loader', exclude: /(node_modules|bower_components)/ },
      { test: /\.json$/, loader: 'json-loader' },
      { test: /\.txt$/, loader: 'raw-loader' },
      { test: /\.(woff|woff2|ttf|eot|otf)(\?v=[0-9]\.[0-9]\.[0-9])?(\?[0-9a-zA-Z]*)?$/, loader: 'file-loader?name=./fonts/[hash].[ext]' },
      { test: /\.(mp4|webm|wav|mp3)(\?v=[0-9]\.[0-9]\.[0-9])?(\?[0-9a-zA-Z]*)?$/, loader: 'file-loader?name=./medias/[hash].[ext]' },
      {
        test: /.*\.(gif|png|jpe?g|svg)$/i,
        loaders: [
          'file-loader?hash=sha512&digest=hex&name=./images/[hash].[ext]',
          {
            loader: 'image-webpack-loader',
            query: {
              mozjpeg: {
                quality: 75,
                progressive: true,
                optimizationLevel: 4,
                interlaced: false,
              },
              pngquant: {
                quality: '65-90',
                speed: 4
              },
              optipng: {
                optimizationLevel: 3
              },
              gifsicle: {
                optimizationLevel: 1
              },
              svgo: {
                plugins: [
                  {
                    removeViewBox: false
                  },
                  {
                    removeEmptyAttrs: false
                  }
                ]
              }
            }
          }
        ]
      },
      {
        test: /\.(scss|sass)$/i,
        // in dev mode, use regular loader
        ...(isDebug ? {
          use: [
            {
                loader: "style-loader?-autoprefixer!postcss-loader" // creates style nodes from JS strings
            }, {
                loader: "css-loader?-autoprefixer!postcss-loader" // translates CSS into CommonJS
            }, {
                loader: "sass-loader?-autoprefixer!postcss-loader" // compiles Sass to CSS
            }, {
              loader: "import-glob-loader" // allow @import path/*;
            }
          ],
        // in prod, extract CSS
        } : {
          use: extractCSS.extract({
            use: [
              {
                loader: "css-loader?-autoprefixer!postcss-loader" // translates CSS into CommonJS
              }, {
                loader: "sass-loader?-autoprefixer!postcss-loader" // compiles Sass to CSS
              }, {
                loader: "import-glob-loader" // allow @import path/*;
              }
            ],
            fallback: "style-loader?-autoprefixer!postcss-loader",
            publicPath: "../"
          })
        })
      },
    ]
  },

  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.$': 'jquery',
      'window.jQuery': 'jquery'
    }),
    new webpack.NamedModulesPlugin(),

    // dev only
    ...(isDebug ? [
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NoEmitOnErrorsPlugin(),
      new webpack.LoaderOptionsPlugin({
       debug: true
     })
    // prod only
    ] : [
      new webpack.optimize.UglifyJsPlugin({
        sourceMap: true,
        parallel: true,
        uglifyOptions : {
          toplevel: true,
          compress: {
            global_defs: {
              "@console.log": "alert"
            },
            passes: 2
          },
          output: {
            beautify: false,
            preamble: "/* uglified */"
          }
        }
      }),
      extractCSS,
      new webpack.DefinePlugin({
        'process.env': {
          NODE_ENV: JSON.stringify('production')
        }
      }),
      new AssetsPlugin({
        path: path.resolve(dist_path),
        publicPath: public_path,
        filename: 'assets.json',
        prettyPrint: true
      }),
      new CleanWebpackPlugin(['js', 'css', '*.jpg', '*.png', '*.svg'], {
        root: path.resolve(dist_path),
        verbose: true,
        dry: false
      })
    ])
  ]

};

export default config;
